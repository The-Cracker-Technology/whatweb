rm -rf /opt/ANDRAX/whatweb/

bundle install

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Bundle install... PASS!"
else
  # houston we have a problem
  exit 1
fi

install -m 755 whatweb.1 /usr/local/share/man/man1/

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Man install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf $(pwd) /opt/ANDRAX/whatweb

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy package... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
